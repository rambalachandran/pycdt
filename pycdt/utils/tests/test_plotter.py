# coding: utf-8

from __future__ import division

__author__ = "Bharat Medasani"
__copyright__ = "Copyright 2014, The Materials Project"
__version__ = "1.0"
__maintainer__ = "Bharat Medasani"
__email__ = "mbkumar@gmail.com"
__status__ = "Development"
__date__ = "May 6, 2015"

import unittest
import os

from pycdt.utils.plotter import DefectPlotter

class DefectPlotterTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_get_plot_form_energy(self):
        pass

    def test_plot_conc_temp(self):
        pass

    def test_plot_carriers_ef(self):
        pass
